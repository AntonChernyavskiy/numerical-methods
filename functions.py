import numpy as np
from math import *
import pickle
from numpy.polynomial.polynomial import polyval


def read(name):
    with open(name, 'rb') as f:
        result = pickle.load(f)
    return result


def remember(result, name):
    with open(name, 'wb') as f:
        pickle.dump(result, f)


def isfloat(x):
    try:
        float(x)
        return 1
    except ValueError:
        return 0


def S(t):
    return 1

def x(t):
    return 1

def z(t):
    return 1

def cor_f(function):
    beta, t, z, x, S = 1, 1, 1, 1, 1
    try:
        eval(function)
        return 1
    except ZeroDivisionError:
        return 1
    except ValueError:
        return 1
    except Exception:
        return 0


def correct(function, state):
    if state == 1:
        w = 1
    else:
        t = 1
    try:
        eval(function)
        return 1
    except ZeroDivisionError:
        return 1
    except ValueError:
        return 1
    except Exception:
        return 0


def function_tabulation(function, state, T):
    result = {}
    if state == 1:
        left_bound = 0
        right_bound = 1
    else:
        left_bound = 0
        right_bound = float(T)
    N = 100
    h = (right_bound - left_bound) / N
    for i in range(0, N + 1):
        if state == 1:
            w = left_bound + i * h
            try:
                result[w] = eval(function)
            except ZeroDivisionError:
                print("Error in w =", w)
            except ValueError:
                print("Error in w =", w)
            remember([result, function], "function_p.dat")
        else:
            t = left_bound + i * h
            try:
                result[t] = eval(function)
            except ZeroDivisionError:
                print("Error in t =", t)
            except ValueError:
                print("Error in t =", t)
            if state == 2:
                remember([result, function], "function_z.dat")
            else:
                remember([result, function], "function_S.dat")


def integral(low_ind, up_ind, f):     # метод трапеций
    vals = [val for val in f.values()]
    points = np.array([key for key in f.keys()])
    h = points[1] - points[0]
    I = 0
    for i in range(low_ind, up_ind + 1):
        I += vals[i]
    return (I - (vals[low_ind] + vals[up_ind]) / 2) * h


def system_solution(a, b, c, f):    # Решение 3-диагональной системы методом прогонки
    n = f.shape[0]
    alpha = np.zeros(n + 1)
    bheta = np.zeros(n + 1)
    for i in range(1, n):
        alpha[i + 1] = -b[i] / (a[i] * alpha[i] + c[i])
        bheta[i + 1] = (f[i - 1] - a[i] * bheta[i]) / (a[i] * alpha[i] + c[i])
    sol = np.zeros(n + 1)
    sol[n] = (f[n - 1] - a[n] * bheta[n]) / (c[n] + a[n] * alpha[n])
    for i in range(n - 1, 0, -1):
        sol[i] = alpha[i + 1] * sol[i + 1] + bheta[i + 1]
    return sol[1:]


def splain_interpolation(func):
    t = np.array([key for key in func.keys()])
    f = np.array([val for val in func.values()])
    n = t.shape[0] - 1
    h = t[1] - t[0]
    a = np.zeros(n)
    b = np.zeros(n)
    m = np.zeros(n + 1)
    m[0] = m[n] = 0

    A = np.zeros(n)
    B = np.zeros(n)
    C = np.zeros(n)
    for i in range(1, n):
        A[i] = B[i] = h / 6
        C[i] = 2 / 3 * h
    right = np.array([(f[i + 2] - 2 * f[i + 1] + f[i]) / h for i in range(n-1)])
    sol = system_solution(A, B, C, right)
    for i in range(1, n):
        m[i] = sol[i - 1]
    for i in range(n):
        a[i] = (f[i + 1] - f[i]) / h - (m[i + 1] - m[i]) / 6 * h
        b[i] = f[i] - m[i] / 6 * h ** 2 - a[i] * t[i]

    res = np.array([np.zeros(4) for _ in range(n)])
    for i in range(n):
        res[i] = np.poly1d([1 / 6 / h]) * (np.poly1d([m[i]]) * (np.poly1d([-1, t[i + 1]]) ** 3) + np.poly1d([m[i + 1]]) * (np.poly1d([1, -t[i]]) ** 3)) + np.poly1d([a[i], b[i]])
    return res


def differentiation(f):
    res = {}
    points = np.array([key for key in f.keys()])
    h = points[1] - points[0]
    N = points.shape[0] - 1
    vals = np.array([val for val in f.values()])
    res[points[0]] = (vals[1] - vals[0]) / h
    for i in range(1, N):
        res[points[i]] = (vals[i + 1] - vals[i - 1]) / 2 / h
    res[points[N]] = (vals[N] - vals[N - 1]) / h
    return res


def calculate_U(p):
    U = {}
    points = [key for key in p.keys()]
    N = len(points) - 1
    for k in range(N + 1):
        U[points[k]] = integral(k, N, p)
    U_coefs = splain_interpolation(U)
    return U_coefs


def calculate_criteria_integral(x, y):
    x_diff = differentiation(x)
    v = {}
    p = read('function_p.dat')[0]
    vals = np.array([val for val in p.values()])
    points = np.array([key for key in p.keys()])
    for i, key in enumerate(points):
        v[key] = (vals * points)[i]
    U_int = calculate_U(v)
    func = {}
    points = np.array([key for key in x.keys()])
    N = len(points) - 1
    for t in points:
        r = 0
        if y[t] > 1:
            r = 1
        elif y[t] != 0:
            r = y[t]
        ind, = np.where(points >= r)
        ind = ind[0]
        if ind != 0:
            ind -= 1
        func[t] = polyval(r, np.flip(U_int[ind], axis=0)) * x_diff[t]
    print(func)
    print(x_diff)
    return integral(0, N, func)


def find_crit(beta, x0, x, y, a=1, b=10, save=True):
    C1 = read("function_C1.dat")
    C2 = read("function_C2.dat")
    S = read('function_S.dat')[0]
    T = max(x.keys())
    I = calculate_criteria_integral(x, y)
    C1[beta] = 1 - 1 / (x[T] - x0) * I
    C2[beta] = abs(x[T] - S[T]) / S[T]
    if save:
        remember(C1, "function_C1.dat")
        remember(C2, "function_C2.dat")
    return a * C1[beta] + b * C2[beta]


def f1(t, x, y):
    z_deriv_coefs = read("interp_deriv_z.dat")
    z = read('function_z.dat')[0]
    points = np.array([key for key in z.keys()])
    ind, = np.where(points >= t)
    ind = ind[0]
    if ind != 0:
        ind -= 1
    z_deriv_val = polyval(t, np.flip(z_deriv_coefs[ind], axis=0))

    p = read('function_p.dat')[0]
    U_int = read('function_U.dat')
    points = np.array([key for key in p.keys()])
    r = 0
    if y > 1:
        r = 1
    elif y < 0:
        r = 0
    else:
        r = y
    ind, = np.where(points >= r)
    ind = ind[0]
    if ind != 0:
        ind -= 1
    U = polyval(r, np.flip(U_int[ind], axis=0))
    return z_deriv_val * U


def f2(t, x, y, beta, f):
    z_coefs = read("interpolation_z.dat")
    S_coefs = read("interpolation_S.dat")
    z = read('function_z.dat')[0]
    points = np.array([key for key in z.keys()])
    ind, = np.where(points >= t)
    ind = ind[0]
    if ind != 0:
        ind -= 1
    z = polyval(t, np.flip(z_coefs[ind], axis=0))
    S = polyval(t, np.flip(S_coefs[ind], axis=0))
    return eval(f)


def runge_kutta(x0, y0, beta, f):
    z = read('function_z.dat')[0]
    t_tau = np.array([key for key in z.keys()])
    x = {}
    y = {}
    x[t_tau[0]] = x0
    y[t_tau[0]] = y0
    for i in range(len(t_tau) - 1):
        tau = (t_tau[i + 1] - t_tau[i])
        m1 = tau * f1(t_tau[i], x[t_tau[i]], y[t_tau[i]])
        k1 = tau * f2(t_tau[i], x[t_tau[i]], y[t_tau[i]], beta, f)
        m2 = tau * f1(t_tau[i] + tau/2, x[t_tau[i]] + m1/2, y[t_tau[i]] + k1/2)
        k2 = tau * f2(t_tau[i] + tau/2, x[t_tau[i]] + m1/2, y[t_tau[i]] + k1/2, beta, f)
        m3 = tau * f1(t_tau[i] + tau/2, x[t_tau[i]] + m2/2, y[t_tau[i]] + k2/2)
        k3 = tau * f2(t_tau[i] + tau/2, x[t_tau[i]] + m2/2, y[t_tau[i]] + k2/2, beta, f)
        m4 = tau * f1(t_tau[i] + tau, x[t_tau[i]] + m3, y[t_tau[i]] + k3)
        k4 = tau * f2(t_tau[i] + tau, x[t_tau[i]] + m3, y[t_tau[i]] + k3, beta, f)
        x[t_tau[i + 1]] = x[t_tau[i]] + (m1 + 2 * m2 + 2 * m3 + m4) / 6
        y[t_tau[i + 1]] = max(y[t_tau[i]] + (k1 + 2 * k2 + 2 * k3 + k4) / 6, 0)

    return x, y


def find_solution(beta, x0, y0, f):
    remember({}, "function_C1.dat")
    remember({}, "function_C2.dat")
    p = read('function_p.dat')[0]
    U_coefs = calculate_U(p)
    remember(U_coefs, 'function_U.dat')

    z = read('function_z.dat')[0]
    S = read('function_S.dat')[0]
    z_coefs = splain_interpolation(z)
    S_coefs = splain_interpolation(S)
    remember(z_coefs, "interpolation_z.dat")
    remember(S_coefs, "interpolation_S.dat")

    z_deriv = differentiation(z)
    z_deriv_coefs = splain_interpolation(z_deriv)
    remember(z_deriv_coefs, "interp_deriv_z.dat")

    x0, y0, beta = float(x0), float(y0), float(beta)
    x, y = runge_kutta(x0, y0, beta, f)
    remember(x, "function_x.dat")
    remember(y, "function_y.dat")
    find_crit(beta, x0, x, y)


def find_beta(beta_range, x0, y0, f):
    opt_beta = None
    F = 10000
    for beta in beta_range:
        x, y = runge_kutta(x0, y0, beta, f)
        cur = find_crit(beta, x0, x, y)
        if opt_beta is None or cur < F:
            remember(x, "function_x.dat")
            remember(y, "function_y.dat")
            opt_beta = beta
            F = cur
    return opt_beta, F


def find_start_conditions(x0_range, y0_range, opt_beta, f, F):
    x0_opt, y0_opt = None, None
    for x0 in x0_range:
        for y0 in y0_range:
            x, y = runge_kutta(x0, y0, opt_beta, f)
            cur = find_crit(opt_beta, x0, x, y, a=0, save=False)
            if y0_opt is None or cur < F:
                remember(x, "function_x.dat")
                remember(y, "function_y.dat")
                x0_opt, y0_opt = x0, y0
                F = cur
    return x0_opt, y0_opt


def find_solution_auto(beta_range, y0, f):
    remember({}, "function_C1.dat")
    remember({}, "function_C2.dat")
    p = read('function_p.dat')[0]
    U_coefs = calculate_U(p)
    remember(U_coefs, 'function_U.dat')

    z = read('function_z.dat')[0]
    S = read('function_S.dat')[0]
    z_coefs = splain_interpolation(z)
    S_coefs = splain_interpolation(S)
    remember(z_coefs, "interpolation_z.dat")
    remember(S_coefs, "interpolation_S.dat")

    z_deriv = differentiation(z)
    z_deriv_coefs = splain_interpolation(z_deriv)
    remember(z_deriv_coefs, "interp_deriv_z.dat")

    x0 = S[min(S.keys())]
    y0 = float(y0)
    left = float(beta_range[0])
    right = float(beta_range[1])
    k = 4
    beta_first_range = (right - left) / k * np.arange(k + 1) + left
    opt_beta, F = find_beta(beta_first_range, x0, y0, f)
    if opt_beta != left and opt_beta != right:
        beta_second_range = (right - left) / (k ** 2) * np.arange(-k/2, k/2 + 1) + opt_beta
    elif opt_beta == left:
        beta_second_range = (right - left) / (2 * k ** 2) * np.arange(k + 1) + opt_beta
    else:
        beta_second_range = (right - left) / (2 * k ** 2) * np.arange(-k, 1) + opt_beta
    opt_beta, F = find_beta(beta_second_range, x0, y0, f)

    x0_max = S[max(S.keys())] / 6
    k = 5
    y0_first_range = np.arange(k + 1) / k * 0.5
    x0_first_range = np.arange(k + 1) * x0_max / k
    x0_opt, y0_opt = find_start_conditions(x0_first_range, y0_first_range, opt_beta, f, F)
    l = 4
    if x0_opt != 0:
        x0_second_range = np.arange(-l / 2, l / 2 + 1) / l / k + x0_opt
    else:
        x0_second_range = np.arange(l + 1) / l / 2 / k + x0_opt
    if y0 != 0 and y0 != 1:
        y0_second_range = np.arange(-l / 2, l / 2 + 1) / l / k * 0.5 + y0_opt
    elif y0 == 0:
        y0_second_range = np.arange(l + 1) / l / 2 / k * 0.5 + y0_opt
    else:
        y0_second_range = np.arange(-l, 1) / l / 2 / k * 0.5 + y0_opt
    x0_opt, y0_opt = find_start_conditions(x0_second_range, y0_second_range, opt_beta, f, F)
    remember([x0_opt, y0_opt, opt_beta], "best_params.dat")
