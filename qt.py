# -*- coding: utf-8 -*-

import sys
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.Qt import QWidget
from functions import *
import matplotlib as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from collections import OrderedDict


class SecondWindow1(QWidget):
    def __init__(self, parent=None,):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle(u'Ручной режим')
        self.resize(1300, 700)
        self.frame = QtWidgets.QFrame(self)

        self.p, self.S, self.z, self.f = "1a", "1a", "1a", "1a"
        self.x0, self.y0, self.T, self.beta = " ", " ", " ", " "

        vbox = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.enter_p = QtWidgets.QLabel(u"Распределение аудитории p(w):", self.frame)
        self.enter_p.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_p)
        self.Yield_p = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_p.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_p)
        self.button_p = QtWidgets.QPushButton(u'Ввести', self)
        self.button_p.clicked.connect(self.test_p)
        hbox.addWidget(self.button_p)
        self.correct_p = QtWidgets.QLabel(u"", self.frame)
        self.correct_p.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_p)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_S = QtWidgets.QLabel(u"План открута S(t):", self.frame)
        self.enter_S.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_S)
        self.Yield_S = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_S.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_S)
        self.button_S = QtWidgets.QPushButton(u'Ввести', self)
        self.button_S.clicked.connect(self.test_S)
        hbox.addWidget(self.button_S)
        self.correct_S = QtWidgets.QLabel(u"", self.frame)
        self.correct_S.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_S)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_z = QtWidgets.QLabel(u"Начальная функция трафика z(t):", self.frame)
        self.enter_z.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_z)
        self.Yield_z = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_z.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_z)
        self.button_z = QtWidgets.QPushButton(u'Ввести', self)
        self.button_z.clicked.connect(self.test_z)
        hbox.addWidget(self.button_z)
        self.correct_z = QtWidgets.QLabel(u"", self.frame)
        self.correct_z.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_z)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_f = QtWidgets.QLabel(u"Функция f(z(t), x(t), S(t), beta):", self.frame)
        self.enter_f.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_f)
        self.Yield_f = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_f.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_f)
        self.button_f = QtWidgets.QPushButton(u'Ввести', self)
        self.button_f.clicked.connect(self.test_f)
        hbox.addWidget(self.button_f)
        self.correct_f = QtWidgets.QLabel(u"", self.frame)
        self.correct_f.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_f)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_x0 = QtWidgets.QLabel(u"Введите начальные условия: x0 ", self.frame)
        self.enter_x0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_x0)
        self.Yield_x0 = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_x0.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_x0)
        self.button_x0 = QtWidgets.QPushButton(u'Ввести', self)
        self.button_x0.clicked.connect(self.test_var_x0)
        hbox.addWidget(self.button_x0)
        self.correct_x0 = QtWidgets.QLabel(u"", self.frame)
        self.correct_x0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_x0)

        self.enter_y0 = QtWidgets.QLabel(u"y0 ", self.frame)
        self.enter_y0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_y0)
        self.Yield_y0 = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_y0.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_y0)
        self.button_y0 = QtWidgets.QPushButton(u'Ввести', self)
        self.button_y0.clicked.connect(self.test_var_y0)
        hbox.addWidget(self.button_y0)
        self.correct_y0 = QtWidgets.QLabel(u"", self.frame)
        self.correct_y0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_y0)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_b = QtWidgets.QLabel(u"Введите beta:", self.frame)
        self.enter_b.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_b)
        self.Yield_b = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_b.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.Yield_b)
        self.button_b = QtWidgets.QPushButton(u'Ввести', self)
        self.button_b.clicked.connect(self.test_var_b)
        hbox.addWidget(self.button_b)
        self.correct_b = QtWidgets.QLabel(u"", self.frame)
        self.correct_b.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_b)

        self.enter_T = QtWidgets.QLabel(u"Введите T:", self.frame)
        self.enter_T.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_T)
        self.Yield_T = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_T.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_T)
        self.button_T = QtWidgets.QPushButton(u'Ввести', self)
        self.button_T.clicked.connect(self.test_var_T)
        hbox.addWidget(self.button_T)
        self.correct_T = QtWidgets.QLabel(u"", self.frame)
        self.correct_T.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_T)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.button_clear = QtWidgets.QPushButton(u'Сбросить введенные данные', self)
        hbox.addWidget(self.button_clear)
        self.button_clear.clicked.connect(self.change)
        self.button_tab = QtWidgets.QPushButton(u'Затабулировать функции p(w), z(t), S(t)', self)
        self.button_tab.clicked.connect(self.tabulate)
        hbox.addWidget(self.button_tab)
        vbox.addLayout(hbox)

        self.button_solve = QtWidgets.QPushButton(u'Найти решение задачи', self)
        hbox.addWidget(self.button_solve)
        self.button_solve.clicked.connect(self.solve)
        vbox.addLayout(hbox)
        hbox = QtWidgets.QHBoxLayout()
        self.tab_res = QtWidgets.QLabel(u"", self.frame)
        self.tab_res.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.tab_res)
        vbox.addLayout(hbox)

        vbox1 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_y = QtWidgets.QPushButton(u'Построить график y(t)', self)
        hbox.addWidget(self.button_graph_y)
        self.button_graph_y.clicked.connect(self.graph_y)
        self.button_graph_C = QtWidgets.QPushButton(u'Построить значения C1(beta), C2(beta)', self)
        self.button_graph_C.clicked.connect(self.graph_C)
        hbox.addWidget(self.button_graph_C)
        vbox1.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas = FigureCanvas(self.fig)
        hbox.addWidget(self.canvas)

        self.fig_1 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_1 = FigureCanvas(self.fig_1)
        hbox.addWidget(self.canvas_1)
        vbox1.addLayout(hbox)

        vbox2 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_p_U = QtWidgets.QPushButton(u'Построить график p(w)', self)
        self.button_graph_p_U.clicked.connect(self.graph_p_U)
        hbox.addWidget(self.button_graph_p_U)
        self.button_graph_z = QtWidgets.QPushButton(u'Построить график z(t)', self)
        self.button_graph_z.clicked.connect(self.graph_z)
        hbox.addWidget(self.button_graph_z)
        vbox2.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig_2 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_2 = FigureCanvas(self.fig_2)
        hbox.addWidget(self.canvas_2)

        self.fig_3 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_3 = FigureCanvas(self.fig_3)
        hbox.addWidget(self.canvas_3)
        vbox2.addLayout(hbox)

        vbox3 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_x_S = QtWidgets.QPushButton(u'Построить графики x(t), S(t), x(t) - S(t)', self)
        self.button_graph_x_S.clicked.connect(self.graph_x_S)
        hbox.addWidget(self.button_graph_x_S)
        self.button_graph_Sx = QtWidgets.QPushButton(u'Построить график S(x)', self)
        self.button_graph_Sx.clicked.connect(self.graph_Sx)
        hbox.addWidget(self.button_graph_Sx)
        vbox3.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig_4 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_4 = FigureCanvas(self.fig_4)
        hbox.addWidget(self.canvas_4)

        self.fig_5 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        # self.axes = self.fig.add_subplot(111)
        self.canvas_5 = FigureCanvas(self.fig_5)
        hbox.addWidget(self.canvas_5)
        vbox3.addLayout(hbox)

        layout = QtWidgets.QGridLayout(self)
        layout.addLayout(vbox, 0, 0)
        layout.addLayout(vbox1, 0, 1)
        layout.addLayout(vbox2, 1, 0)
        layout.addLayout(vbox3, 1, 1)


    def solve(self):
        k = 0
        unknown = "Следующие параметры (функция) не введены или введены неверно: "
        if isfloat(self.beta) == 0 or self.beta.isspace():
            unknown += "beta "
            k += 1
        if isfloat(self.x0) == 0 or self.x0.isspace():
            unknown += "x0 "
            k += 1
        if isfloat(self.y0) == 0 or self.y0.isspace():
            unknown += "y0 "
            k += 1
        if cor_f(self.f) == 0:
            unknown += "f "
            k += 1
        if k == 0:
            find_solution(self.beta, self.x0, self.y0, self.f)
            self.tab_res.setText(u"<font color=blue><b> \t\t Задача решена <b></font>")
        else:
            self.tab_res.setText(u"<font color=red><b>" + unknown + " <b></font>")

    def graph_p_U(self):
        self.axes_2 = self.fig_2.add_subplot(111)
        self.axes_2.clear()
        p = read("function_p.dat")
        dict = p[0]
        keys = np.array([key for key in dict.keys()])
        vals = np.array([val for val in dict.values()])
        self.axes_2.set_xlabel(u"w")
        self.axes_2.plot(keys, vals, label='p(w) = ' + p[1])
        self.axes_2.grid(True)
        self.axes_2.legend()
        self.canvas_2.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график p(w) = " + p[1] + "<b></font>")

    def graph_z(self):
        self.axes_3 = self.fig_3.add_subplot(111)
        self.axes_3.clear()
        data1 = read("function_z.dat")
        dictionary = data1[0]
        keys = np.array([key for key in dictionary.keys()])
        vals = np.array([val for val in dictionary.values()])
        self.axes_3.grid(True)
        self.axes_3.plot(keys, vals, label='z(t) = ' + data1[1])
        self.axes_3.legend()
        self.canvas_3.draw()
        self.tab_res.setText(
            u"<font color=blue><b> \t\t Построен график z(t) = " + data1[1] + " <b></font>")

    def graph_y(self):
        self.axes = self.fig.add_subplot(111)
        self.axes.clear()
        y_points = read("function_y.dat")
        y_keys = np.array([key for key in y_points.keys()])
        y_vals = np.array([val for val in y_points.values()])
        self.axes.set_xlabel(u"t")
        self.axes.grid(True)
        self.axes.plot(y_keys, y_vals, label='y(t)')
        self.axes.legend()
        self.canvas.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график y(t) <b></font>")

    def graph_x_S(self):
        self.axes_4 = self.fig_4.add_subplot(111)
        self.axes_4.clear()
        x_points = read("function_x.dat")
        S = read("function_S.dat")
        S_points = S[0]
        x_keys = np.array([key for key in x_points.keys()])
        S_keys = np.array([key for key in S_points.keys()])
        x_vals = np.array([val for val in x_points.values()])
        S_vals = np.array([val for val in S_points.values()])
        self.axes_4.set_xlabel(u"t")
        self.axes_4.grid(True)
        self.axes_4.plot(x_keys, x_vals, label='x(t)')
        self.axes_4.plot(x_keys, x_vals - S_vals, label='x(t) - S(t)')
        self.axes_4.plot(S_keys, S_vals, label='S(t) = ' + S[1])
        self.axes_4.legend()
        self.canvas_4.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построены графики x(t); x(t)-S(t); S(t) = " + S[1] + "<b></font>")

    def graph_Sx(self):
        self.axes_5 = self.fig_5.add_subplot(111)
        self.axes_5.clear()
        x_points = read("function_x.dat")
        S_points = read("function_S.dat")[0]
        x_vals = np.array([val for val in x_points.values()])
        S_vals = np.array([val for val in S_points.values()])
        self.axes_5.set_xlabel(u"x")
        self.axes_5.grid(True)
        self.axes_5.plot(x_vals, S_vals, label='S(x)')
        self.axes_5.legend()
        self.canvas_5.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график S(x) <b></font>")

    def graph_C(self):
        self.axes_1 = self.fig_1.add_subplot(111)
        self.axes_1.clear()
        C1 = read("function_C1.dat")
        keys = np.array([key for key in C1.keys()])
        vals = np.array([val for val in C1.values()])
        self.axes_1.set_xlabel(u"beta")
        self.axes_1.plot(keys, vals, marker='o', label='C1(beta)')
        self.canvas_1.draw()

        C2 = read("function_C2.dat")
        keys = np.array([key for key in C2.keys()])
        vals = np.array([val for val in C2.values()])
        self.axes_1.grid(True)
        self.axes_1.plot(keys, vals, marker='o', label='C2(beta)')
        self.axes_1.legend()
        self.canvas_1.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построены значения С1(beta), C2(beta) <b></font>")

    def change(self):
        self.correct_p.setText(u"")
        self.correct_z.setText(u"")
        self.correct_S.setText(u"")
        self.correct_f.setText(u"")
        self.correct_x0.setText(u"")
        self.correct_y0.setText(u"")
        self.correct_T.setText(u"")
        self.correct_b.setText(u"")
        self.Yield_p.setText(u"")
        self.Yield_z.setText(u"")
        self.Yield_S.setText(u"")
        self.Yield_f.setText(u"")
        self.Yield_x0.setText(u"")
        self.Yield_y0.setText(u"")
        self.Yield_T.setText(u"")
        self.Yield_b.setText(u"")
        self.tab_res.setText(u"<font color=blue><b> Введенные данные сброшены <b></font>")
        self.p, self.S, self.z, self.f = "1a", "1a", "1a", "1a"
        self.x0, self.y0, self.T, self.beta = " ", " ", " ", " "

    def test_f(self):
        entering = self.Yield_f.displayText()
        res = cor_f(entering)
        if res != 0:
            self.f = entering
            self.correct_f.setText(u"<font color=black> f = " + entering + "</font>")
        else:
            self.correct_f.setText(u"<font color=red> Некорректный ввод </font>")
            self.f = "1a"

    def test_p(self):
        entering = self.Yield_p.displayText()
        res = correct(entering, 1)
        if res != 0:
            self.p = entering
            self.correct_p.setText(u"<font color=black> p(w) = " + entering + "</font>")
        else:
            self.correct_p.setText(u"<font color=red> Некорректный ввод </font>")
            self.p = "1a"

    def test_z(self):
        entering = self.Yield_z.displayText()
        res = correct(entering, 2)
        if res != 0:
            self.z = entering
            self.correct_z.setText(u"<font color=black> z(t) = " + entering + "</font>")
        else:
            self.correct_z.setText(u"<font color=red> Некорректный ввод </font>")
            self.z = "1a"

    def test_S(self):
        entering = self.Yield_S.displayText()
        res = correct(entering, 3)
        if res != 0:
            self.S = entering
            self.correct_S.setText(u"<font color=black> S(t) = " + entering + "</font>")
        else:
            self.correct_S.setText(u"<font color=red> Некорректный ввод </font>")
            self.S = "1a"

    def test_var_x0(self):
        entering = self.Yield_x0.displayText()
        if isfloat(entering):
            self.x0 = entering
            self.correct_x0.setText(u"<font color=black> x0 = " + entering + ";  </font>")
        else:
            self.x0 = " "
            self.correct_x0.setText(u"<font color=red> Некорректный ввод </font>")

    def test_var_y0(self):
        entering = self.Yield_y0.displayText()
        if isfloat(entering):
            if float(entering) > 1 or float(entering) < 0:
                self.y0 = " "
                self.correct_y0.setText(u"<font color=red> ! y0 не из [0, 1] ! </font>")
            else:
                self.y0 = entering
                self.correct_y0.setText(u"<font color=black> y0 = " + entering + " </font>")
        else:
            self.y0 = " "
            self.correct_y0.setText(u"<font color=red> Некорректный ввод </font>")

    def test_var_b(self):
        entering = self.Yield_b.displayText()
        if isfloat(entering):
            self.beta = entering
            self.correct_b.setText(u"<font color=black> beta = " + entering + " </font>")
        else:
            self.beta = " "
            self.correct_b.setText(u"<font color=red> Некорректный ввод </font>")

    def test_var_T(self):
        entering = self.Yield_T.displayText()
        if isfloat(entering):
            self.T = entering
            self.correct_T.setText(u"<font color=black> T = " + entering + " </font>")
        else:
            self.T = " "
            self.correct_T.setText(u"<font color=red> Некорректный ввод </font>")

    def tabulate(self):
        unknown = "Следующие функции (параметр) не введены или введены неверно: "
        k = 0
        if correct(self.p, 1) == 0 or self.p.isspace():
            unknown += "p(w) "
            k += 1
        if correct(self.z, 2) == 0 or self.z.isspace():
            unknown += "z(t) "
            k += 1
        if correct(self.S, 3) == 0 or self.S.isspace():
            unknown += "S(t) "
            k += 1
        if self.T.isdigit():
            if k == 0:
                function_tabulation(self.S, 3, self.T)
                function_tabulation(self.z, 2, self.T)
                function_tabulation(self.p, 1, self.T)
                self.tab_res.setText(u"<font color=blue><b> \t\t Функции затабулированы <b></font>")
                return
        else:
            unknown += "T "
        self.tab_res.setText(u"<font color=red><b>" + unknown +" <b></font>")


class SecondWindow2(QWidget):
    def __init__(self, parent=None,):
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle(u'Автоматический режим')
        self.resize(1300, 700)
        self.frame = QtWidgets.QFrame(self)

        self.p, self.S, self.z, self.f = "1a", "1a", "1a", "1a"
        self.x0, self.y0, self.T, self.beta = " ", " ", " ", [" ", " "]

        vbox = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.enter_p = QtWidgets.QLabel(u"Распределение аудитории p(w):", self.frame)
        self.enter_p.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_p)
        self.Yield_p = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_p.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_p)
        self.button_p = QtWidgets.QPushButton(u'Ввести', self)
        self.button_p.clicked.connect(self.test_p)
        hbox.addWidget(self.button_p)
        self.correct_p = QtWidgets.QLabel(u"", self.frame)
        self.correct_p.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_p)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_S = QtWidgets.QLabel(u"План открута S(t):", self.frame)
        self.enter_S.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_S)
        self.Yield_S = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_S.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_S)
        self.button_S = QtWidgets.QPushButton(u'Ввести', self)
        self.button_S.clicked.connect(self.test_S)
        hbox.addWidget(self.button_S)
        self.correct_S = QtWidgets.QLabel(u"", self.frame)
        self.correct_S.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_S)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_z = QtWidgets.QLabel(u"Начальная функция трафика z(t):", self.frame)
        self.enter_z.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_z)
        self.Yield_z = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_z.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_z)
        self.button_z = QtWidgets.QPushButton(u'Ввести', self)
        self.button_z.clicked.connect(self.test_z)
        hbox.addWidget(self.button_z)
        self.correct_z = QtWidgets.QLabel(u"", self.frame)
        self.correct_z.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_z)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_f = QtWidgets.QLabel(u"Функция f(z(t), x(t), S(t), beta):", self.frame)
        self.enter_f.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_f)
        self.Yield_f = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_f.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_f)
        self.button_f = QtWidgets.QPushButton(u'Ввести', self)
        self.button_f.clicked.connect(self.test_f)
        hbox.addWidget(self.button_f)
        self.correct_f = QtWidgets.QLabel(u"", self.frame)
        self.correct_f.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_f)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_y0 = QtWidgets.QLabel(u"Введите y0 ", self.frame)
        self.enter_y0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_y0)
        self.Yield_y0 = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_y0.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_y0)
        self.button_y0 = QtWidgets.QPushButton(u'Ввести', self)
        self.button_y0.clicked.connect(self.test_var_y0)
        hbox.addWidget(self.button_y0)
        self.correct_y0 = QtWidgets.QLabel(u"", self.frame)
        self.correct_y0.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_y0)

        self.enter_T = QtWidgets.QLabel(u"Введите T:", self.frame)
        self.enter_T.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_T)
        self.Yield_T = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_T.setFont(QtGui.QFont("Arial", 10))
        hbox.addWidget(self.Yield_T)
        self.button_T = QtWidgets.QPushButton(u'Ввести', self)
        self.button_T.clicked.connect(self.test_var_T)
        hbox.addWidget(self.button_T)
        self.correct_T = QtWidgets.QLabel(u"", self.frame)
        self.correct_T.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_T)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.enter_b = QtWidgets.QLabel(u"Введите границы для beta:", self.frame)
        self.enter_b.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.enter_b)
        self.Yield_bleft = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_bleft.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.Yield_bleft)
        self.Yield_bright = QtWidgets.QLineEdit(u"", self.frame)
        self.Yield_bright.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.Yield_bright)
        self.button_b = QtWidgets.QPushButton(u'Ввести', self)
        self.button_b.clicked.connect(self.test_var_b)
        hbox.addWidget(self.button_b)
        self.correct_b = QtWidgets.QLabel(u"", self.frame)
        self.correct_b.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.correct_b)
        vbox.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.button_clear = QtWidgets.QPushButton(u'Сбросить введенные данные', self)
        hbox.addWidget(self.button_clear)
        self.button_clear.clicked.connect(self.change)
        self.button_tab = QtWidgets.QPushButton(u'Затабулировать функции p(w), z(t), S(t)', self)
        self.button_tab.clicked.connect(self.tabulate)
        hbox.addWidget(self.button_tab)
        vbox.addLayout(hbox)

        self.button_solve = QtWidgets.QPushButton(u'Найти решение задачи', self)
        hbox.addWidget(self.button_solve)
        self.button_solve.clicked.connect(self.solve)
        vbox.addLayout(hbox)
        hbox = QtWidgets.QHBoxLayout()
        self.tab_res = QtWidgets.QLabel(u"", self.frame)
        self.tab_res.setFont(QtGui.QFont("Times new roman", 13))
        hbox.addWidget(self.tab_res)
        vbox.addLayout(hbox)

        vbox1 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_C1 = QtWidgets.QPushButton(u'Построить графики C1(beta), C2(beta)', self)
        hbox.addWidget(self.button_graph_C1)
        self.button_graph_C1.clicked.connect(self.graph_C1)
        self.button_graph_C2 = QtWidgets.QPushButton(u'Построить график y(t)', self)
        self.button_graph_C2.clicked.connect(self.graph_C2)
        hbox.addWidget(self.button_graph_C2)
        vbox1.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas = FigureCanvas(self.fig)
        hbox.addWidget(self.canvas)

        self.fig_1 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_1 = FigureCanvas(self.fig_1)
        hbox.addWidget(self.canvas_1)
        vbox1.addLayout(hbox)

        vbox2 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_p_U = QtWidgets.QPushButton(u'Построить график p(w)', self)
        self.button_graph_p_U.clicked.connect(self.graph_p_U)
        hbox.addWidget(self.button_graph_p_U)
        self.button_graph_z_y = QtWidgets.QPushButton(u'Построить график z(t)', self)
        self.button_graph_z_y.clicked.connect(self.graph_z_y)
        hbox.addWidget(self.button_graph_z_y)
        vbox2.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig_2 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_2 = FigureCanvas(self.fig_2)
        hbox.addWidget(self.canvas_2)

        self.fig_3 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_3 = FigureCanvas(self.fig_3)
        hbox.addWidget(self.canvas_3)
        vbox2.addLayout(hbox)

        vbox3 = QtWidgets.QVBoxLayout()
        hbox = QtWidgets.QHBoxLayout()
        self.button_graph_x_S = QtWidgets.QPushButton(u'Построить графики x(t), S(t), x(t) - S(t)', self)
        self.button_graph_x_S.clicked.connect(self.graph_x_S)
        hbox.addWidget(self.button_graph_x_S)
        self.button_graph_Sx = QtWidgets.QPushButton(u'Построить график S(x)', self)
        self.button_graph_Sx.clicked.connect(self.graph_Sx)
        hbox.addWidget(self.button_graph_Sx)
        vbox3.addLayout(hbox)

        hbox = QtWidgets.QHBoxLayout()
        self.fig_4 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        self.canvas_4 = FigureCanvas(self.fig_4)
        hbox.addWidget(self.canvas_4)

        self.fig_5 = plt.figure.Figure(figsize=(6, 6), dpi=65)
        # self.axes = self.fig.add_subplot(111)
        self.canvas_5 = FigureCanvas(self.fig_5)
        hbox.addWidget(self.canvas_5)
        vbox3.addLayout(hbox)

        layout = QtWidgets.QGridLayout(self)
        layout.addLayout(vbox, 0, 0)
        layout.addLayout(vbox1, 0, 1)
        layout.addLayout(vbox2, 1, 0)
        layout.addLayout(vbox3, 1, 1)

    def solve(self):
        k = 0
        unknown = "Следующие параметры не введены или введены неверно: "
        left = self.beta[0]
        right = self.beta[1]
        if isfloat(left) == 0 or left.isspace() or isfloat(right) == 0 or right.isspace():
            unknown += "beta "
            k += 1
        if isfloat(self.y0) == 0 or self.y0.isspace():
            unknown += "y0 "
            k += 1
        if cor_f(self.f) == 0:
            unknown += "f "
            k += 1
        if k == 0:
            find_solution_auto(self.beta, self.y0, self.f)
            self.tab_res.setText(u"<font color=blue><b> \t\t Задача решена <b></font>")
        else:
            self.tab_res.setText(u"<font color=red><b>" + unknown + " <b></font>")

    def graph_p_U(self):
        self.axes_2 = self.fig_2.add_subplot(111)
        self.axes_2.clear()
        p = read("function_p.dat")
        dict = p[0]
        keys = np.array([key for key in dict.keys()])
        vals = np.array([val for val in dict.values()])
        self.axes_2.set_xlabel(u"w")
        self.axes_2.plot(keys, vals, label='p(w) = ' + p[1])
        self.axes_2.grid(True)
        self.axes_2.legend()
        self.canvas_2.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график p(w) = " + p[1] + "<b></font>")

    def graph_z_y(self):
        self.axes_3 = self.fig_3.add_subplot(111)
        self.axes_3.clear()
        data1 = read("function_z.dat")
        dictionary = data1[0]
        keys = np.array([key for key in dictionary.keys()])
        vals = np.array([val for val in dictionary.values()])
        self.axes_3.plot(keys, vals, label='z(t) = ' + data1[1])
        self.axes_3.set_xlabel(u"t")
        self.axes_3.grid(True)
        self.axes_3.legend()
        self.canvas_3.draw()
        self.tab_res.setText(
            u"<font color=blue><b> \t\t Построены графики z(t) = " + data1[1] + "<b></font>")


    def graph_x_S(self):
        self.axes_4 = self.fig_4.add_subplot(111)
        self.axes_4.clear()
        x_points = read("function_x.dat")
        S = read("function_S.dat")
        S_points = S[0]
        x_keys = np.array([key for key in x_points.keys()])
        S_keys = np.array([key for key in S_points.keys()])
        x_vals = np.array([val for val in x_points.values()])
        S_vals = np.array([val for val in S_points.values()])
        self.axes_4.set_xlabel(u"t")
        self.axes_4.grid(True)
        self.axes_4.plot(x_keys, x_vals, label='x(t)')
        self.axes_4.plot(x_keys, x_vals - S_vals, label='x(t) - S(t)')
        self.axes_4.plot(S_keys, S_vals, label='S(t) = ' + S[1])
        [x0_opt, y0_opt, opt_beta] = read("best_params.dat")
        self.axes_4.set_title('opt. x0 = ' + str(round(x0_opt, 3)) + "; opt. y0 = " + str(round(y0_opt, 3)) + "; opt. beta = " + str(round(opt_beta, 3)))
        self.axes_4.legend()
        self.canvas_4.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построены графики x(t); x(t)-S(t); S(t) = " + S[1] + "<b></font>")

    def graph_Sx(self):
        self.axes_5 = self.fig_5.add_subplot(111)
        self.axes_5.clear()
        x_points = read("function_x.dat")
        S_points = read("function_S.dat")[0]
        x_vals = np.array([val for val in x_points.values()])
        S_vals = np.array([val for val in S_points.values()])
        self.axes_5.set_xlabel(u"x")
        self.axes_5.grid(True)
        self.axes_5.plot(x_vals, S_vals, label='S(x)')
        self.axes_5.legend()
        self.canvas_5.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график S(x) <b></font>")

    def graph_C1(self):
        self.axes = self.fig.add_subplot(111)
        self.axes.clear()
        C1 = read("function_C1.dat")
        keys = np.array([key for key in C1.keys()])
        keys = np.sort(keys)
        vals_1 = np.array([C1[key] for key in keys])
        self.axes.set_xlabel(u"beta")
        self.axes.plot(keys, vals_1, marker='o', label='C1(beta)')
        C2 = read("function_C2.dat")
        keys = np.array([key for key in C2.keys()])
        keys = np.sort(keys)
        vals = np.array([C2[key] for key in keys])
        self.axes.plot(keys, vals, marker='o', label='C2(beta)')
        self.axes.plot(keys, vals_1 / 10 + vals, marker='o', label='0.1C1 + C2')
        self.axes.grid(True)
        self.axes.legend()
        self.canvas.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построены графики С1(beta), С2(beta)<b></font>")

    def graph_C2(self):
        self.axes_1 = self.fig_1.add_subplot(111)
        self.axes_1.clear()
        y_points = read("function_y.dat")
        y_keys = np.array([key for key in y_points.keys()])
        y_vals = np.array([val for val in y_points.values()])
        self.axes_1.plot(y_keys, y_vals, label='y(t)')
        self.axes_1.grid(True)
        self.axes_1.set_xlabel(u"t")
        self.axes_1.legend()
        self.canvas_1.draw()
        self.tab_res.setText(u"<font color=blue><b> \t\t Построен график y(t) <b></font>")

    def change(self):
        self.correct_p.setText(u"")
        self.correct_f.setText(u"")
        self.correct_z.setText(u"")
        self.correct_S.setText(u"")
        self.correct_y0.setText(u"")
        self.correct_T.setText(u"")
        self.correct_b.setText(u"")
        self.Yield_p.setText(u"")
        self.Yield_z.setText(u"")
        self.Yield_f.setText(u"")
        self.Yield_S.setText(u"")
        self.Yield_y0.setText(u"")
        self.Yield_T.setText(u"")
        self.Yield_bleft.setText(u"")
        self.Yield_bright.setText(u"")
        self.tab_res.setText(u"<font color=blue><b> Введенные данные сброшены <b></font>")
        self.p, self.S, self.z = "1a", "1a", "1a"
        self.x0, self.y0, self.T, self.beta = " ", " ", " ", [" ", " "]

    def test_f(self):
        entering = self.Yield_f.displayText()
        res = cor_f(entering)
        if res != 0:
            self.f = entering
            self.correct_f.setText(u"<font color=black> f = " + entering + "</font>")
        else:
            self.correct_f.setText(u"<font color=red> Некорректный ввод </font>")
            self.f = "1a"

    def test_p(self):
        entering = self.Yield_p.displayText()
        res = correct(entering, 1)
        if res != 0:
            self.p = entering
            self.correct_p.setText(u"<font color=black> p(w) = " + entering + "</font>")
        else:
            self.correct_p.setText(u"<font color=red> Некорректный ввод </font>")
            self.p = "1a"

    def test_z(self):
        entering = self.Yield_z.displayText()
        res = correct(entering, 2)
        if res != 0:
            self.z = entering
            self.correct_z.setText(u"<font color=black> z(t) = " + entering + "</font>")
        else:
            self.correct_z.setText(u"<font color=red> Некорректный ввод </font>")
            self.z = "1a"

    def test_S(self):
        entering = self.Yield_S.displayText()
        res = correct(entering, 3)
        if res != 0:
            self.S = entering
            self.correct_S.setText(u"<font color=black> S(t) = " + entering + "</font>")
        else:
            self.correct_S.setText(u"<font color=red> Некорректный ввод </font>")
            self.S = "1a"


    def test_var_y0(self):
        entering = self.Yield_y0.displayText()
        if isfloat(entering):
            if float(entering) > 1 or float(entering) < 0:
                self.y0 = " "
                self.correct_y0.setText(u"<font color=red> ! y0 не из [0, 1] ! </font>")
            else:
                self.y0 = entering
                self.correct_y0.setText(u"<font color=black> y0 = " + entering + " </font>")
        else:
            self.y0 = " "
            self.correct_y0.setText(u"<font color=red> Некорректный ввод </font>")

    def test_var_b(self):
        entering_left = self.Yield_bleft.displayText()
        entering_right = self.Yield_bright.displayText()
        if isfloat(entering_left) and isfloat(entering_right):
            if float(entering_left) > float(entering_right):
                self.beta = " "
                self.correct_b.setText(u"<font color=red> Некорректный ввод </font>")
            else:
                self.beta = [entering_left, entering_right]
                self.correct_b.setText(u"<font color=black> beta из [" + entering_left + "; " + entering_right + "] </font>")
        else:
            self.beta = " "
            self.correct_b.setText(u"<font color=red> Некорректный ввод </font>")

    def test_var_T(self):
        entering = self.Yield_T.displayText()
        if isfloat(entering):
            self.T = entering
            self.correct_T.setText(u"<font color=black> T = " + entering + " </font>")
        else:
            self.T = " "
            self.correct_T.setText(u"<font color=red> Некорректный ввод </font>")

    def tabulate(self):
        unknown = "Следующие функции (параметр) не введены или введены неверно: "
        k = 0
        if correct(self.p, 1) == 0 or self.p.isspace():
            unknown += "p(w) "
            k += 1
        if correct(self.z, 2) == 0 or self.z.isspace():
            unknown += "z(t) "
            k += 1
        if correct(self.S, 3) == 0 or self.S.isspace():
            unknown += "S(t) "
            k += 1
        if self.T.isdigit():
            if k == 0:
                function_tabulation(self.S, 3, self.T)
                function_tabulation(self.z, 2, self.T)
                function_tabulation(self.p, 1, self.T)
                self.tab_res.setText(u"<font color=blue><b> \t\t Функции затабулированы <b></font>")
                return
        else:
            unknown += "T "
        self.tab_res.setText(u"<font color=red><b>" + unknown + " <b></font>")


class Main_window(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.secondWin1 = None
        self.secondWin2 = None
        QtWidgets.QWidget.__init__(self, parent)
        self.setWindowTitle(u'Динамическая настройка параметров в социально-демографическом рекламном сервере')
        self.resize(680, 150)

        self.frame = QtWidgets.QFrame(self)
        vbox = QtWidgets.QVBoxLayout()
        self.var = QtWidgets.QLabel(u"<i>Выберете режим запуска:<i>", self.frame)
        self.var.setFont(QtGui.QFont("Times new roman", 15))
        vbox.addWidget(self.var)

        self.button1 = QtWidgets.QPushButton(u'Ручной режим', self.frame)
        self.button1.setFont(QtGui.QFont("Arial", 13, QtGui.QFont.Bold))
        self.button1.clicked.connect(self.openWin1)
        vbox.addWidget(self.button1)

        self.button2 = QtWidgets.QPushButton(u'Автоматический режим', self.frame)
        self.button2.setFont(QtGui.QFont("Arial", 13, QtGui.QFont.Bold))
        self.button2.clicked.connect(self.openWin2)
        vbox.addWidget(self.button2)

        self.setLayout(vbox)

    def openWin1(self):
        self.secondWin1 = SecondWindow1(self)
        self.secondWin1.show()

    def openWin2(self):
        self.secondWin2 = SecondWindow2(self)
        self.secondWin2.show()

    def closeEvent(self, event):
        reply = QtWidgets.QMessageBox.question(self, u'Выход',
        u"Вы уверены, что хотите выйти?", QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


app = QtWidgets.QApplication(sys.argv)
app.setStyleSheet(open("./style.qss","r").read())
window = Main_window()
window.show()
sys.exit(app.exec_())
